package com.example.origamishop.infraestructure.adapter.payu;

import java.util.HashMap;
import java.util.Map;

import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.model.Status;
import com.example.origamishop.domain.usecases.payment.RefundService;
import com.example.origamishop.infraestructure.adapter.exception.PayuRefundException;
import org.springframework.stereotype.Component;

import com.payu.sdk.PayU;
import com.payu.sdk.PayUPayments;
import com.payu.sdk.model.TransactionResponse;

@Component
public class PayuRefundedAdapter extends PayuConnection implements RefundService {

	@Override
	public Order refundOrder(Order order){
		try{
			this.connectWithPayuSdk();
			Map<String, String> parameters = new HashMap<String, String>();

			parameters.put(PayU.PARAMETERS.ORDER_ID, Integer.toString(order.getPaymentNumber()));
			parameters.put(PayU.PARAMETERS.TRANSACTION_ID, order.getTransaction());

			TransactionResponse response = PayUPayments.doVoid(parameters);

			if (response != null){
				order.setStatus(Status.REFUNDED);
			}
			return order;

		}catch (Exception e){
			throw new PayuRefundException("Error while the refund process is being carried out", e);
		}
	}
}
