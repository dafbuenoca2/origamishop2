package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.ProductService;
import com.example.origamishop.infraestructure.adapter.persistence.dto.ProductEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductAdapter implements ProductService {

	private ProductDbRepository productDbRepository;
	private ModelMapper mapper;

	public ProductAdapter(final ProductDbRepository productDbRepository, CategoryDbRepository categoryDbRepository, ModelMapper mapper) {

		this.productDbRepository = productDbRepository;
		this.mapper = mapper;
	}

	@Override
	public List<Product> getProducts(){

		List<Product> productList = new ArrayList<Product>();
		List<ProductEntity> productEntityList = productDbRepository.findAll();
		for (ProductEntity productEntity : productEntityList) {
			Product product = this.toProductModel(productEntity);
			productList.add(product);
		}
		return productList;
	}

	@Override
	public Product getProductById(String productId){
		Optional<ProductEntity> productEntity = productDbRepository.findById(productId);
		Product product = this.toProductModel(productEntity.get());
		return product;
	}

	@Override
	public Product createProduct(Product product){
		ProductEntity productEntity = this.toProductEntity(product);
		productDbRepository.save(productEntity);
		return product;
	}

	@Override
	public  Product deleteProduct(String productId){
		Product product = new Product();
		if (productDbRepository.existsById(productId)){
			ProductEntity productEntity = productDbRepository.getById(productId);
			productDbRepository.delete(productEntity);
			product = this.toProductModel(productEntity);
		}
		return product;
	}

	@Override
	public Product updateProduct(String productId, Product product){
		Product updatedProduct = new Product();
		if (productDbRepository.existsById(productId)){
			ProductEntity productEntity = this.toProductEntity(product);
			productDbRepository.save(productEntity);
			updatedProduct = product;
		}
		return  updatedProduct;
	}

	@Override
	public List<Product> getProductsByCategory(String categoryId){
		List<Product> productList = new ArrayList<>();
		List<ProductEntity> productEntityList = productDbRepository.findByCategoryCategoryId(categoryId);
		for (ProductEntity productEntity : productEntityList){
			Product product = this.toProductModel(productEntity);
			productList.add(product);
		}
		return  productList;
	}

	public ProductEntity toProductEntity(Product product){
		ProductEntity productEntity = mapper.map(product, ProductEntity.class);
		return productEntity;
	}

	public Product toProductModel(ProductEntity productEntity){
		Product product = mapper.map(productEntity, Product.class);
		return product;
	}
}
