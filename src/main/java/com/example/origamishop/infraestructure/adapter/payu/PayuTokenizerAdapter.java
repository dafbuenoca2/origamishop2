package com.example.origamishop.infraestructure.adapter.payu;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.example.origamishop.domain.model.CreditCard;
import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.payment.TokenizerService;
import com.example.origamishop.infraestructure.adapter.exception.PayUTokenizationException;
import org.springframework.stereotype.Component;

import com.payu.sdk.PayU;
import com.payu.sdk.PayUTokens;
import com.payu.sdk.model.CreditCardToken;

@Component
public class PayuTokenizerAdapter extends PayuConnection implements TokenizerService{

	@Override
	public CreditCardTokenize tokenizeCreditCard(CreditCard creditCard, Customer customer){
		try{
			this.connectWithPayuSdk();
			CreditCardTokenize creditCardTokenize =  new CreditCardTokenize();

			Map<String, String> parameters = new HashMap<String, String>();

			parameters.put(PayU.PARAMETERS.PAYER_NAME, customer.getFullName());
			parameters.put(PayU.PARAMETERS.PAYER_ID, customer.getCustomerId());
			parameters.put(PayU.PARAMETERS.PAYER_DNI, customer.getDniNumber());
			parameters.put(PayU.PARAMETERS.CREDIT_CARD_NUMBER, creditCard.getNumber());
			parameters.put(PayU.PARAMETERS.CREDIT_CARD_EXPIRATION_DATE, creditCard.getExpiration());
			parameters.put(PayU.PARAMETERS.PAYMENT_METHOD, creditCard.getPaymentMethod());

			CreditCardToken response = PayUTokens.create(parameters);
			if(response != null){
				creditCardTokenize.setCreditCardId(UUID.randomUUID().toString());
				creditCardTokenize.setToken(response.getTokenId());
				creditCardTokenize.setAlias(creditCard.getAlias());
				creditCardTokenize.setProvider("PAYU");
	
			}
			return creditCardTokenize;
		}catch (Exception e){
			throw new PayUTokenizationException("Error, while tokenize credit card process", e);
		}
	}
}
