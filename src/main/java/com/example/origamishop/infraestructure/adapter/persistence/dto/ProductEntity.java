package com.example.origamishop.infraestructure.adapter.persistence.dto;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "products")
public class ProductEntity {

	@Id
	@Column(name = "product_id", nullable = false)
	private String productId;

	@Column
	@NotNull
	private String name;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "category_id", nullable = false)
	private CategoryEntity category;

	@Column
	@NotNull
	@Min(value = 50)
	@Max(value = 1000000)
	private Integer price;

	@Column
	private String description;

	@Column
	@NotNull
	@Min(value = 0)
	private Integer stock;

}
