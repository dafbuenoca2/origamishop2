package com.example.origamishop.infraestructure.adapter.exception;

public class PayUTokenizationException extends RuntimeException{

	public PayUTokenizationException(final String message, final Throwable cause) {

		super(message, cause);
	}
}
