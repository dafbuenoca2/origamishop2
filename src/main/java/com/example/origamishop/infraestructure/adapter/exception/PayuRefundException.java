package com.example.origamishop.infraestructure.adapter.exception;

public class PayuRefundException extends RuntimeException{

	public PayuRefundException(final String message, final Throwable cause) {

		super(message, cause);
	}
}
