package com.example.origamishop.infraestructure.adapter.persistence;

import com.example.origamishop.domain.model.Item;
import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.usecases.item.ItemService;
import com.example.origamishop.infraestructure.adapter.persistence.dto.ItemEntity;
import com.example.origamishop.infraestructure.adapter.persistence.dto.OrderEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ItemAdapter implements ItemService {

	private ItemDbRepository itemDbRepository;
	private ModelMapper mapper;

	public ItemAdapter(final ItemDbRepository itemDbRepository, final ModelMapper mapper) {

		this.itemDbRepository = itemDbRepository;
		this.mapper = mapper;
	}

	@Override
	public Item createItem(Item item, Order order){
		ItemEntity itemEntity =  this.toItemEntity(item);
		OrderEntity orderEntity = this.mapper.map(order, OrderEntity.class);
		itemEntity.setOrder(orderEntity);
		itemDbRepository.save(itemEntity);
		return item;
	}

	public ItemEntity toItemEntity(Item item){
		return this.mapper.map(item, ItemEntity.class);
	}

	public Item toItemModel(ItemEntity itemEntity){
		return this.mapper.map(itemEntity, Item.class);
	}
}
