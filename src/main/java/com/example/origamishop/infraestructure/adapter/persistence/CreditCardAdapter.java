package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.creditcard.CreditCardService;
import com.example.origamishop.infraestructure.adapter.persistence.dto.CreditCardEntity;
import com.example.origamishop.infraestructure.adapter.persistence.dto.CustomerEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CreditCardAdapter implements CreditCardService {

	private CreditCardDbRepository creditCardDbRepository;
	private ModelMapper mapper;

	public CreditCardAdapter(final CreditCardDbRepository creditCardDbRepository, final ModelMapper mapper) {

		this.creditCardDbRepository = creditCardDbRepository;
		this.mapper = mapper;
	}

	@Override
	public CreditCardTokenize createCreditCard(CreditCardTokenize creditCardTokenize, Customer customer){

		CreditCardEntity creditCardEntity = this.toCreditCardEntity(creditCardTokenize);
		CustomerEntity customerEntity = this.mapper.map(customer, CustomerEntity.class);
		creditCardEntity.setCustomer(customerEntity);
		this.creditCardDbRepository.save(creditCardEntity);
		return  creditCardTokenize;
	}

	@Override
	public CreditCardTokenize getCreditCardId(String creditCardTokenizeId){

		Optional<CreditCardEntity> creditCardEntity = this.creditCardDbRepository.findById(creditCardTokenizeId);
		CreditCardTokenize creditCardTokenize = this.toCreditCardModel(creditCardEntity.get());
		return creditCardTokenize;
	}

	@Override
	public List<CreditCardTokenize> getCreditCardsByCustomer(String customerId) {
		List<CreditCardTokenize> creditCardTokenizeList = new ArrayList<>();
		List<CreditCardEntity> creditCardEntityList = this.creditCardDbRepository.findByCustomerCustomerId(customerId);
		for (CreditCardEntity creditCardEntity : creditCardEntityList){
			CreditCardTokenize creditCardTokenize = this.toCreditCardModel(creditCardEntity);
			creditCardTokenizeList.add(creditCardTokenize);
		}
		return creditCardTokenizeList;
	}

	public CreditCardEntity toCreditCardEntity(CreditCardTokenize creditCardTokenize){

		return this.mapper.map(creditCardTokenize, CreditCardEntity.class);
	}

	public CreditCardTokenize toCreditCardModel(CreditCardEntity creditCardEntity){

		return this.mapper.map(creditCardEntity, CreditCardTokenize.class);
	}
}
