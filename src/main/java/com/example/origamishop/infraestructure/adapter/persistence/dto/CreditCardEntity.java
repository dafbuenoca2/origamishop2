package com.example.origamishop.infraestructure.adapter.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name= "credit_cards")
public class CreditCardEntity {

	@Id
	@Column(name = "credit_card_id", nullable = false)
	private String creditCardId;

	@Column
	@NotNull
	private String token;

	@Column
	@NotNull
	private String alias;

	@Column
	@NotNull
	private String provider;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "customer_id", nullable = false)
	private CustomerEntity customer;

}
