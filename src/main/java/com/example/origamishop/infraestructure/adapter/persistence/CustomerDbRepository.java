package com.example.origamishop.infraestructure.adapter.persistence;

import com.example.origamishop.infraestructure.adapter.persistence.dto.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerDbRepository extends JpaRepository<CustomerEntity, String> {

}
