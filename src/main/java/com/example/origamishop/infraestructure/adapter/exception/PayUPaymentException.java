package com.example.origamishop.infraestructure.adapter.exception;

public class PayUPaymentException extends RuntimeException{

	public PayUPaymentException(final String message, final Throwable cause) {

		super(message, cause);
	}
}
