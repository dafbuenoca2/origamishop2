package com.example.origamishop.infraestructure.adapter.persistence.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "categories")
public class CategoryEntity {

	@Id
	@Column(name = "category_id", nullable = false)
	private String categoryId;

	@Column(name = "name")
	@NotEmpty(message = "Please provide a name")
	private  String name;

	@Column(name = "description")
	private  String description;


}
