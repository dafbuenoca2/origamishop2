package com.example.origamishop.infraestructure.adapter.persistence;

import com.example.origamishop.infraestructure.adapter.persistence.dto.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDbRepository extends JpaRepository<CategoryEntity, String> {

}
