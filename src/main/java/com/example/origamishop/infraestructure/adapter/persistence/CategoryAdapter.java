package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.usecases.category.CategoryService;
import com.example.origamishop.infraestructure.adapter.persistence.dto.CategoryEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CategoryAdapter implements CategoryService {

	private  CategoryDbRepository categoryDbRepository;
	private ModelMapper mapper;;

	public CategoryAdapter(final CategoryDbRepository categoryDbRepository, ModelMapper mapper) {

		this.categoryDbRepository = categoryDbRepository;
		this.mapper = mapper;
	}

	@Override
	public List<Category> getCategories(){

		List<Category> categoryList = new ArrayList<Category>();
		List<CategoryEntity> categoryEntityList = categoryDbRepository.findAll();
		for (CategoryEntity categoryEntity : categoryEntityList) {
			Category category = this.toCategoryModel(categoryEntity);
			categoryList.add(category);
		}
		return categoryList;
	}

	@Override
	public Category getCategoryById(String categoryId){
		Optional<CategoryEntity> categoryEntity = categoryDbRepository.findById(categoryId);
		Category category = this.toCategoryModel(categoryEntity.get());
		return category;
	}

	@Override
	public Category addCategory(Category category){
		CategoryEntity categoryEntity = this.toCategoryEntity(category);
		categoryDbRepository.save(categoryEntity);
		return category;
	}

	@Override
	public  Category removeCategory(String categoryId){
		Category category = new Category();
		if (categoryDbRepository.existsById(categoryId)){
			CategoryEntity categoryEntity = categoryDbRepository.findById(categoryId).get();
			category = this.toCategoryModel(categoryEntity);
			categoryDbRepository.delete(categoryEntity);
		}
		return category;
	}

	public CategoryEntity toCategoryEntity(Category category){
		CategoryEntity categoryEntity = mapper.map(category, CategoryEntity.class);
		return categoryEntity;
	}

	public Category toCategoryModel(CategoryEntity categoryEntity){
		Category category = mapper.map(categoryEntity, Category.class);
		return category;
	}
}
