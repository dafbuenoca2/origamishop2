package com.example.origamishop.infraestructure.adapter.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.example.origamishop.domain.model.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class OrderEntity {

	@Id
	@Column(name = "order_id", nullable = false)
	private String orderId;

	@NotNull
	@Min(value = 0)
	private Integer price;

	@NotNull
	private Status status;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "customer_id", nullable = false)
	private CustomerEntity customer;

	@Column(name = "transaction")
	private  String transaction;

	private  Integer  paymentNumber;
}
