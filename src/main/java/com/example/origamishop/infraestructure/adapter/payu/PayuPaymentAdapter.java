package com.example.origamishop.infraestructure.adapter.payu;

import java.util.HashMap;
import java.util.Map;

import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.model.Status;
import com.example.origamishop.domain.usecases.payment.PaymentService;
import com.example.origamishop.infraestructure.adapter.exception.PayUPaymentException;
import org.springframework.stereotype.Component;

import com.payu.sdk.PayU;
import com.payu.sdk.PayUPayments;
import com.payu.sdk.model.Currency;
import com.payu.sdk.model.Language;
import com.payu.sdk.model.PaymentCountry;
import com.payu.sdk.model.TransactionResponse;

@Component
public class PayuPaymentAdapter extends PayuConnection implements PaymentService {

	@Override
	public Order paymentOrder(Customer customer, Order order, CreditCardTokenize cardTokenize){
		try{

			this.connectWithPayuSdk();

			Map<String, String> parameters = new HashMap<String, String>();

			parameters.put(PayU.PARAMETERS.ACCOUNT_ID, "512321");
			parameters.put(PayU.PARAMETERS.REFERENCE_CODE, order.getOrderId());
			parameters.put(PayU.PARAMETERS.DESCRIPTION, "payment test");

			parameters.put(PayU.PARAMETERS.VALUE, Integer.toString(order.getPrice()));
			parameters.put(PayU.PARAMETERS.CURRENCY, ""+ Currency.COP.name());

			parameters.put(PayU.PARAMETERS.BUYER_ID, customer.getCustomerId());
			parameters.put(PayU.PARAMETERS.BUYER_NAME, customer.getFullName());
			parameters.put(PayU.PARAMETERS.BUYER_EMAIL, customer.getEmail());
			parameters.put(PayU.PARAMETERS.BUYER_CONTACT_PHONE, customer.getPhone());
			parameters.put(PayU.PARAMETERS.BUYER_DNI, customer.getDniNumber());
			parameters.put(PayU.PARAMETERS.BUYER_STREET, customer.getAddress());
			parameters.put(PayU.PARAMETERS.BUYER_CITY, customer.getCity());
			parameters.put(PayU.PARAMETERS.BUYER_STATE, customer.getState());
			parameters.put(PayU.PARAMETERS.BUYER_COUNTRY, "CO");

			parameters.put(PayU.PARAMETERS.PAYER_ID, customer.getCustomerId());
			parameters.put(PayU.PARAMETERS.PAYER_NAME, customer.getFullName());
			parameters.put(PayU.PARAMETERS.PAYER_EMAIL, customer.getEmail());
			parameters.put(PayU.PARAMETERS.PAYER_CONTACT_PHONE, customer.getPhone());
			parameters.put(PayU.PARAMETERS.PAYER_DNI, customer.getDniNumber());
			parameters.put(PayU.PARAMETERS.PAYER_STREET, customer.getAddress());
			parameters.put(PayU.PARAMETERS.PAYER_STREET_2, customer.getAddress());
			parameters.put(PayU.PARAMETERS.PAYER_CITY, customer.getCity());
			parameters.put(PayU.PARAMETERS.PAYER_STATE, customer.getState());
			parameters.put(PayU.PARAMETERS.PAYER_COUNTRY, "CO");

			parameters.put(PayU.PARAMETERS.TOKEN_ID, cardTokenize.getToken());

			parameters.put(PayU.PARAMETERS.INSTALLMENTS_NUMBER, "1");
			parameters.put(PayU.PARAMETERS.COUNTRY, PaymentCountry.CO.name());

			TransactionResponse response = PayUPayments.doAuthorizationAndCapture(parameters);

			if(response != null){
				order.setPaymentNumber(response.getOrderId());
				order.setTransaction(response.getTransactionId());
				order.setStatus(Status.FINALIZED);
			}

			return order;
		}catch (Exception e){
			throw  new PayUPaymentException("Error while the payment process is being carried out" , e);
		}
	}
}
