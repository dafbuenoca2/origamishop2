package com.example.origamishop.infraestructure.adapter.persistence;

import com.example.origamishop.infraestructure.adapter.persistence.dto.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemDbRepository extends JpaRepository<ItemEntity, String> {

}
