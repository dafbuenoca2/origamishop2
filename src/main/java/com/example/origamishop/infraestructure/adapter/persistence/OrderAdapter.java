package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.usecases.order.OrderService;
import com.example.origamishop.infraestructure.adapter.persistence.dto.CustomerEntity;
import com.example.origamishop.infraestructure.adapter.persistence.dto.OrderEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderAdapter implements OrderService {

	private OrderDbRepository orderDbRepository;
	private ModelMapper mapper;

	public OrderAdapter(final OrderDbRepository orderDbRepository, final ModelMapper mapper) {

		this.orderDbRepository = orderDbRepository;
		this.mapper = mapper;
	}

	@Override
	public Order createOrder(Order order, Customer customer){
		OrderEntity orderEntity =  this.toOrderEntity(order);
		CustomerEntity customerEntity = this.mapper.map(customer, CustomerEntity.class);
		orderEntity.setCustomer(customerEntity);
		this.orderDbRepository.save(orderEntity);
		return order;
	}

	@Override
	public List<Order> getOrdersByCustomer(String customerId) {

		List<Order> orderList = new ArrayList<>();
		List<OrderEntity> orderEntityList = orderDbRepository.findByCustomerCustomerId(customerId);
		for (OrderEntity orderEntity : orderEntityList) {
			Order order = this.toOrderModel(orderEntity);
			orderList.add(order);
		}
		return orderList;
	}

	@Override
	public Order getOrderById(String orderId){

		Optional<OrderEntity> orderEntity = this.orderDbRepository.findById(orderId);
		Order order = this.toOrderModel(orderEntity.get());
		return order;
	}


	public OrderEntity toOrderEntity(Order order){

		return this.mapper.map(order, OrderEntity.class);
	}

	public Order toOrderModel(OrderEntity orderEntity){

		return this.mapper.map(orderEntity, Order.class);
	}
}
