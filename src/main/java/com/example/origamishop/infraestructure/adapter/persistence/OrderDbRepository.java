package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.List;

import com.example.origamishop.infraestructure.adapter.persistence.dto.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDbRepository extends JpaRepository<OrderEntity, String> {

	List<OrderEntity> findByCustomerCustomerId(String customerId);
}
