package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.List;

import com.example.origamishop.infraestructure.adapter.persistence.dto.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDbRepository extends JpaRepository<ProductEntity, String> {

	List<ProductEntity> findByCategoryCategoryId(String categoryId);
}
