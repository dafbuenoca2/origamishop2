package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.Optional;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.customer.CustomerService;
import com.example.origamishop.infraestructure.adapter.persistence.dto.CustomerEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerAdapter implements CustomerService {

	private CustomerDbRepository customerDbRepository;
	private ModelMapper mapper;

	public CustomerAdapter(final CustomerDbRepository customerDbRepository, final ModelMapper mapper) {

		this.customerDbRepository = customerDbRepository;
		this.mapper = mapper;
	}

	@Override
	public Customer createCustomer(Customer customer){
		CustomerEntity customerEntity =  this.toCustomerEntity(customer);
		customerDbRepository.save(customerEntity);
		return customer;
	}

	@Override
	public Customer getCustomerById(String customerId){
		Optional<CustomerEntity> customerEntity = customerDbRepository.findById(customerId);
		Customer customer = this.toCustomerModel(customerEntity.get());	
		return customer;
	}

	public CustomerEntity toCustomerEntity(Customer customer){

		return this.mapper.map(customer, CustomerEntity.class);
	}

	public Customer toCustomerModel(CustomerEntity customerEntity){

		return this.mapper.map(customerEntity, Customer.class);
	}
}
