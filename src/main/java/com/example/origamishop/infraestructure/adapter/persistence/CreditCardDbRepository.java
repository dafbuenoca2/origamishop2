package com.example.origamishop.infraestructure.adapter.persistence;

import java.util.List;

import com.example.origamishop.infraestructure.adapter.persistence.dto.CreditCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditCardDbRepository extends JpaRepository<CreditCardEntity, String> {

	List<CreditCardEntity> findByCustomerCustomerId(String customerId);
}
