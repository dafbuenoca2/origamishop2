package com.example.origamishop.infraestructure.adapter.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Items")
public class ItemEntity {

	@Id
	@Column(name = "item_id", nullable = false)
	private String itemId;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "product_id", nullable = false)
	private ProductEntity product;

	@ManyToOne(optional = false)
	@NotNull
	@JoinColumn(name = "order_id", nullable = false)
	private OrderEntity order;

	@NotNull
	@Min(value = 1)
	private Integer amount;
}
