package com.example.origamishop.infraestructure.adapter.persistence.dto;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customers")
public class CustomerEntity {

	@Id
	@Column(name = "customer_id", nullable = false)
	private  String customerId;

	@Column(name = "dni_type")
	@NotEmpty(message = "Please provide a DNI Type")
	private String dniType;

	@Column(name = "dni_number")
	@NotEmpty(message = "Please provide a ")
	private String dniNumber;

	@Column(name = "email")
	@Email
	@NotEmpty(message = "Please provide a email")
	private String email;

	@Column(name = "first_name")
	@NotEmpty(message = "Please provide a first name")
	private String firstName;

	@Column(name = "last_name")
	@NotEmpty(message = "Please provide a last name")
	private String lastName;

	@Column(name = "state")
	@NotEmpty(message = "Please provide a state")
	private String state;

	@Column(name = "city")
	@NotEmpty(message = "Please provide a city")
	private String city;

	@Column(name = "Address")
	@NotEmpty(message = "Please provide a Address")
	private String Address;

	@Column(name = "phone")
	@NotEmpty(message = "Please provide a phone")
	private String phone;

}
