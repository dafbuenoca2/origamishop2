package com.example.origamishop.entrypoint.adapter.web.category;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.usecases.category.RemoveCategoryInputPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class DeleteCategoryController {

	private RemoveCategoryInputPort removeCategoryInputPort;

	@Autowired
	public DeleteCategoryController(RemoveCategoryInputPort removeCategoryInputPort){

		this.removeCategoryInputPort = removeCategoryInputPort;
	}

	@DeleteMapping("/{categoryId}")
	public ResponseEntity<Category> removeCategory(@PathVariable String categoryId){

		return new ResponseEntity<>(removeCategoryInputPort.removeCategory(categoryId), HttpStatus.OK);
	}
}
