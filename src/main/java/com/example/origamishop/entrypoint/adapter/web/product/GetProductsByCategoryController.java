package com.example.origamishop.entrypoint.adapter.web.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.GetPruductsByCategoryInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class GetProductsByCategoryController {

	private GetPruductsByCategoryInputPort getPruductsByCategoryInputPort;

	public GetProductsByCategoryController(
			final GetPruductsByCategoryInputPort getPruductsByCategoryInputPort) {

		this.getPruductsByCategoryInputPort = getPruductsByCategoryInputPort;
	}

	@GetMapping("/{categoryId}/products")
	public ResponseEntity<List<Product>> getProductsByCategory(@PathVariable String categoryId){

		return  new ResponseEntity<>(getPruductsByCategoryInputPort.getProductsByCategory(categoryId), HttpStatus.OK);
	}

}
