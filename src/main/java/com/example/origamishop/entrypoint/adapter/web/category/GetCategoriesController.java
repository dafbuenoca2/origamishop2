package com.example.origamishop.entrypoint.adapter.web.category;

import java.util.List;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.usecases.category.CategoriesInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/categories")
public class GetCategoriesController {

	private CategoriesInputPort categoriesInputPort;

	public GetCategoriesController(CategoriesInputPort categoriesInputPort){

		this.categoriesInputPort = categoriesInputPort;
	}

	@GetMapping
	public ResponseEntity<List<Category>> getCategories(){
		return new ResponseEntity<>(categoriesInputPort.getCategories(), HttpStatus.OK);
	}
}
