package com.example.origamishop.entrypoint.adapter.web.creditcard;

import javax.validation.Valid;

import com.example.origamishop.domain.model.CreditCard;
import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.usecases.creditcard.CreateCreditCardInputPort;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCreditCardRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers")
public class CreateCreditCardController {

	private CreateCreditCardInputPort createCreditCardInputPort;

	public CreateCreditCardController(final CreateCreditCardInputPort createCreditCardInputPort) {

		this.createCreditCardInputPort = createCreditCardInputPort;
	}

	@PostMapping("/{customerId}/creditCards")
	public ResponseEntity<CreditCardTokenize> createCreditCard(@PathVariable String customerId,
															   @Valid @RequestBody CreateCreditCardRequest createCreditCardRequest){

		return new ResponseEntity<>(createCreditCardInputPort.createCreditCard(customerId, createCreditCardRequest), HttpStatus.CREATED);
	}
}
