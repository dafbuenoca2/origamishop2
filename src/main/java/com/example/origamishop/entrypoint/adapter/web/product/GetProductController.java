package com.example.origamishop.entrypoint.adapter.web.product;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.GetProductInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/products")
public class GetProductController {

	private GetProductInputPort productInputPort;

	public GetProductController(GetProductInputPort productInputPort){

		this.productInputPort = productInputPort;
	}

	@GetMapping("/{productId}")
	public ResponseEntity<Product> getProductById(@PathVariable String productId){

		return new ResponseEntity<>(productInputPort.getProductById(productId), HttpStatus.OK);
	}
}
