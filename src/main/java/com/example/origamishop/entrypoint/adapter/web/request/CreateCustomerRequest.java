package com.example.origamishop.entrypoint.adapter.web.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.example.origamishop.domain.model.DniType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateCustomerRequest {

	@NotNull
	private DniType dniType;

	@NotNull
	private String dniNumber;

	@NotNull
	@Email
	private String email;

	@NotNull
	private String firstName;

	@NotNull
	private  String lastName;

	@NotNull
	private  String state;

	@NotNull
	private String city;

	@NotNull
	private  String address;

	@NotNull
	private  String phone;
}


