package com.example.origamishop.entrypoint.adapter.web.order;

import javax.validation.Valid;

import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.usecases.order.CreateOrderInputPort;
import com.example.origamishop.entrypoint.adapter.web.request.CreateOrderRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers")
public class CreateOrderController {

	private CreateOrderInputPort createOrderInputPort;

	public CreateOrderController(final CreateOrderInputPort createOrderInputPort) {

		this.createOrderInputPort = createOrderInputPort;
	}

	@PostMapping("/{customerId}/orders")
	public ResponseEntity<Order> createOrder(@PathVariable String customerId, @Valid @RequestBody CreateOrderRequest createOrderRequest){

		return new ResponseEntity<>(createOrderInputPort.createOrder(customerId, createOrderRequest), HttpStatus.CREATED);
	}
}
