package com.example.origamishop.entrypoint.adapter.web.customer;

import javax.validation.Valid;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.customer.CreateCustomerInputPort;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCustomerRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("api/customers")
public class CreateCustomerController {

	private CreateCustomerInputPort createCustomerInputPort;

	public CreateCustomerController(final CreateCustomerInputPort createCustomerInputPort) {

		this.createCustomerInputPort = createCustomerInputPort;
	}

	@PostMapping
	public ResponseEntity<Customer> createCustomer(@Valid @RequestBody CreateCustomerRequest createCustomerRequest){

		return new ResponseEntity<>(createCustomerInputPort.createCustomer(createCustomerRequest), HttpStatus.CREATED);
	}
}
