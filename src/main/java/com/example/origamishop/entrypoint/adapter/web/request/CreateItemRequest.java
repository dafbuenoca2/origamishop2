package com.example.origamishop.entrypoint.adapter.web.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateItemRequest {

	@NotNull
	private String productId;

	@NotNull
	@Min(value = 1)
	private Integer amount;
}
