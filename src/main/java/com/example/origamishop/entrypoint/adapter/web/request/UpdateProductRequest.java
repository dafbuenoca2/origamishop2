package com.example.origamishop.entrypoint.adapter.web.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProductRequest {

	private String name;

	private String description;

	@Min(value = 50)
	@Max(value = 1000000)
	private Integer price;

	@Min(value = 0)
	private Integer stock;

	private String category;

}
