package com.example.origamishop.entrypoint.adapter.web.request;

import java.util.List;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderRequest {

	@NotNull
	private List<CreateItemRequest> items;

	@NotNull
	private String creditCard;
}
