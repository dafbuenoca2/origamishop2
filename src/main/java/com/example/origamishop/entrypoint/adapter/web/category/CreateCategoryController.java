package com.example.origamishop.entrypoint.adapter.web.category;

import javax.validation.Valid;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.usecases.category.CreateCategoryInputPort;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCategoryRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/categories")
public class CreateCategoryController {

	private CreateCategoryInputPort createCategoryInputPort;

	public CreateCategoryController(CreateCategoryInputPort createCategoryInputPort){

		this.createCategoryInputPort = createCategoryInputPort;
	}

	@PostMapping
	public ResponseEntity<Category> createCategory(@Valid @RequestBody CreateCategoryRequest createCategoryRequest){

		return new ResponseEntity<>(createCategoryInputPort.addCategory(createCategoryRequest), HttpStatus.CREATED);
	}
}
