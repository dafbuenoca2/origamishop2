package com.example.origamishop.entrypoint.adapter.web.product;

import javax.validation.Valid;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.CreateProductInputPort;
import com.example.origamishop.entrypoint.adapter.web.request.CreateProductRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class CreateProductController {

	private CreateProductInputPort createProductInputPort;

	public CreateProductController(CreateProductInputPort createProductInputPort) {

		this.createProductInputPort = createProductInputPort;
	}

	@PostMapping("/api/products")
	public ResponseEntity<Product> createProduct(@Valid @RequestBody CreateProductRequest createProductRequest){

		return new ResponseEntity<>(createProductInputPort.createProduct(createProductRequest), HttpStatus.CREATED);
	}
}
