package com.example.origamishop.entrypoint.adapter.web.order;

import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.usecases.order.RefundOrderInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers")
public class RefundOrderController {

	RefundOrderInputPort refundOrderInputPort;

	public RefundOrderController(final RefundOrderInputPort refundOrderInputPort) {

		this.refundOrderInputPort = refundOrderInputPort;
	}

	@PutMapping("/{customerId}/orders/{orderId}")
	public ResponseEntity<Order> refundOrder(@PathVariable String orderId, @PathVariable String customerId){

		return new ResponseEntity<>(this.refundOrderInputPort.refundOrder(orderId, customerId), HttpStatus.OK);
	}

}
