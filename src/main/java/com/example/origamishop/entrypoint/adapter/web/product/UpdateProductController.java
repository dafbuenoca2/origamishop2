package com.example.origamishop.entrypoint.adapter.web.product;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.UpdateProductInputPort;
import com.example.origamishop.entrypoint.adapter.web.request.UpdateProductRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UpdateProductController {

	private UpdateProductInputPort updateProductInputPort;

	public UpdateProductController(final UpdateProductInputPort updateProductInputPort) {

		this.updateProductInputPort = updateProductInputPort;
	}

	@PutMapping("/api/products/{productId}")
	public ResponseEntity<Product> updateProduct(@PathVariable String productId, @RequestBody UpdateProductRequest updateProductRequest){

		return new ResponseEntity<>(updateProductInputPort.updateProduct(productId, updateProductRequest), HttpStatus.OK);
	}
}
