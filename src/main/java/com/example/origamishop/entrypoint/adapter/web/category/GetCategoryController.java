package com.example.origamishop.entrypoint.adapter.web.category;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.usecases.category.CategoryInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class GetCategoryController {

	private CategoryInputPort categoryInputPort;

	public GetCategoryController(CategoryInputPort categoryInputPort){

		this.categoryInputPort = categoryInputPort;
	}

	@GetMapping("/{categoryId}")
	public ResponseEntity<Category> getCategoryById(@PathVariable String categoryId){

		return new ResponseEntity<>(categoryInputPort.getCategoryById(categoryId), HttpStatus.OK);
	}
}
