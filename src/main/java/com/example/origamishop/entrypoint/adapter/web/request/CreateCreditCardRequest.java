package com.example.origamishop.entrypoint.adapter.web.request;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateCreditCardRequest {

	@NotNull
	private  String alias;

	@NotNull
	private  String paymentMethod;

	@NotNull
	private  String numberCreditCard;

	@NotNull
	private  String expiration;
}


