package com.example.origamishop.entrypoint.adapter.web.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.GetProductsInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/products")
public class GetProductsController {

	private GetProductsInputPort productsInputPort;

	public GetProductsController(GetProductsInputPort productsInputPort){

		this.productsInputPort = productsInputPort;
	}

	@GetMapping
	public ResponseEntity<List<Product>> getProducts(){
		return new ResponseEntity<>(productsInputPort.getProducts(), HttpStatus.OK);
	}
}
