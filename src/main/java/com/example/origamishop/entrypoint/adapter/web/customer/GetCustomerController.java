package com.example.origamishop.entrypoint.adapter.web.customer;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.customer.GetCustomerInputPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api/customers")
public class GetCustomerController {

	private GetCustomerInputPort customerInputPort;

	public GetCustomerController(GetCustomerInputPort customerInputPort){

		this.customerInputPort = customerInputPort;
	}

	@GetMapping("/{customerId}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable String customerId){

		return new ResponseEntity<>(customerInputPort.getCustomerById(customerId), HttpStatus.OK);
	}
}
