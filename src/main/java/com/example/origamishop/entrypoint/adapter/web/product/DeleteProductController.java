package com.example.origamishop.entrypoint.adapter.web.product;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.product.DeleteProductInputPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product")
public class DeleteProductController {

	private DeleteProductInputPort deleteProductInputPort;

	@Autowired
	public DeleteProductController(DeleteProductInputPort deleteProductInputPort){

		this.deleteProductInputPort = deleteProductInputPort;
	}

	@DeleteMapping("/{productId}")
	public ResponseEntity<Product> deleteProduct(@PathVariable String productId){

		return new ResponseEntity<>(deleteProductInputPort.deleteProduct(productId), HttpStatus.OK);
	}
}
