package com.example.origamishop.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with", toBuilder = true)
public class CreditCard {

	private String creditCardId;
	private String paymentMethod;
	private String alias;
	private String number;
	private String expiration;

}
