package com.example.origamishop.domain.model;

public enum Status { INITIALIZED, CANCELLED, FINALIZED, REFUNDED, INSUFFICIENT_PRODUCTS}
