package com.example.origamishop.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with", toBuilder = true)
public class Product {

	private String productId;
	private String name;
	private Category category;
	private Integer price;
	private String description;
	private Integer stock;

	public void modifyStock(Integer amount){
		this.stock = this.stock + amount;
	}
}
