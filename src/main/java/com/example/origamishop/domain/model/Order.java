package com.example.origamishop.domain.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with", toBuilder = true)
public class Order {

	private String orderId;
	private List<Item> items;
	private Integer price;
	private Status status;
	private String transaction;
	private Integer paymentNumber;

	public void setPrice() {
		Integer price = 0;
		for (Item item : items){
			price += item.getProduct().getPrice() * item.getAmount();
		}
		this.price = price;
	}
}
