package com.example.origamishop.domain.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with", toBuilder = true)
public class Customer {

	private String customerId;
	private DniType dniType;
	private String dniNumber;
	private String email;
	private String firstName;
	private String lastName;
	private String state;
	private String city;
	private String Address;
	private String phone;
	private List<Order> orders;
	private List<CreditCardTokenize> creditCardList;

	public String getFullName(){
		return  this.firstName + " " + this.lastName;
	}
}
