package com.example.origamishop.domain.model;

public enum DniType { CC,CE, NIT, TI, PP, IDC, CEL, RC, DE}
