package com.example.origamishop.domain.usecases.customer;

import com.example.origamishop.domain.model.Customer;

public interface GetCustomerInputPort {

	Customer getCustomerById(String customerId);
}
