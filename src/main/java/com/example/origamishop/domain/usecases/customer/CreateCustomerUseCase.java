package com.example.origamishop.domain.usecases.customer;

import java.util.UUID;

import com.example.origamishop.domain.model.CreditCard;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCustomerRequest;
import org.springframework.stereotype.Service;

@Service
public class CreateCustomerUseCase implements CreateCustomerInputPort{

	private CustomerService customerService;

	public CreateCustomerUseCase(final CustomerService customerService) {

		this.customerService = customerService;
	}

	@Override
	public Customer createCustomer(CreateCustomerRequest createCustomerRequest){

		Customer customer = Customer.builder()
				.withCustomerId(UUID.randomUUID().toString())
				.withDniType(createCustomerRequest.getDniType())
				.withDniNumber(createCustomerRequest.getDniNumber())
				.withEmail(createCustomerRequest.getEmail())
				.withFirstName(createCustomerRequest.getFirstName())
				.withLastName(createCustomerRequest.getLastName())
				.withState(createCustomerRequest.getState())
				.withCity(createCustomerRequest.getCity())
				.withAddress(createCustomerRequest.getAddress())
				.withPhone(createCustomerRequest.getPhone())
				.build();
		return this.customerService.createCustomer(customer);
	}
}
