package com.example.origamishop.domain.usecases.product;

import java.util.UUID;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.category.CategoryService;
import com.example.origamishop.entrypoint.adapter.web.request.CreateProductRequest;
import org.springframework.stereotype.Service;

@Service
public class CreateProductUseCase implements CreateProductInputPort{

	private ProductService productService;
	private CategoryService categoryService;

	public CreateProductUseCase(final ProductService productService, CategoryService categoryService) {

		this.productService = productService;
		this.categoryService = categoryService;
	}

	@Override
	public Product createProduct(CreateProductRequest createProductRequest){

		Category category = categoryService.getCategoryById(createProductRequest.getCategory());
		Product product = Product.builder().withProductId(UUID.randomUUID().toString())
				.withName(createProductRequest.getName())
				.withDescription(createProductRequest.getDescription())
				.withPrice(createProductRequest.getPrice())
				.withStock(createProductRequest.getStock())
				.withCategory(category)
				.build();
		return productService.createProduct(product);
	}
}
