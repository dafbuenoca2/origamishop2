package com.example.origamishop.domain.usecases.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;
import org.springframework.stereotype.Service;

@Service
public class GetProductByCategoryUseCase implements  GetPruductsByCategoryInputPort{

	private ProductService productService;

	public GetProductByCategoryUseCase(final ProductService productService) {

		this.productService = productService;
	}

	@Override
	public List<Product> getProductsByCategory(String categoryId){

		return this.productService.getProductsByCategory(categoryId);
	}
}
