package com.example.origamishop.domain.usecases.category;

import com.example.origamishop.domain.model.Category;
import org.springframework.stereotype.Service;

@Service
public class RemoveCategoryUseCase implements RemoveCategoryInputPort {

	private CategoryService categoryService;

	public RemoveCategoryUseCase(final CategoryService categoryService) {

		this.categoryService = categoryService;
	}

	@Override
	public Category removeCategory(String categoryId) {

		return categoryService.removeCategory(categoryId);
	}
}
