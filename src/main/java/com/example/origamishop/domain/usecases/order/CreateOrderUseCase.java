package com.example.origamishop.domain.usecases.order;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.model.Item;
import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.model.Status;
import com.example.origamishop.domain.usecases.creditcard.CreditCardService;
import com.example.origamishop.domain.usecases.customer.CustomerService;
import com.example.origamishop.domain.usecases.item.ItemService;
import com.example.origamishop.domain.usecases.payment.PaymentService;
import com.example.origamishop.domain.usecases.product.ProductService;
import com.example.origamishop.entrypoint.adapter.web.request.CreateItemRequest;
import com.example.origamishop.entrypoint.adapter.web.request.CreateOrderRequest;
import org.springframework.stereotype.Service;

@Service
public class CreateOrderUseCase implements CreateOrderInputPort{

	private ProductService productService;
	private ItemService itemService;
	private OrderService orderService;
	private CustomerService customerService;
	private CreditCardService creditCardService;
	private PaymentService paymentService;

	public CreateOrderUseCase(final ProductService productService, final ItemService itemService, final  OrderService orderService,
							  final CustomerService customerService, final CreditCardService creditCardService, final PaymentService paymentService) {

		this.productService = productService;
		this.itemService = itemService;
		this.orderService = orderService;
		this.customerService = customerService;
		this.creditCardService = creditCardService;
		this.paymentService = paymentService;
	}

	@Override
	public Order createOrder(String customerId, CreateOrderRequest createOrderRequest){

		Customer customer = this.customerService.getCustomerById(customerId);
		Order order = new Order();
		order.setOrderId(UUID.randomUUID().toString());
		order.setStatus(Status.INITIALIZED);
		order.setPrice(0);
		order = this.orderService.createOrder(order, customer);
		List<CreateItemRequest> createItemRequestList = createOrderRequest.getItems();
		order = this.createProductList(order, createItemRequestList);
		CreditCardTokenize creditCardTokenize = this.creditCardService.getCreditCardId(createOrderRequest.getCreditCard());
		if (order.getStatus().equals(Status.INITIALIZED)){
			order = this.paymentService.paymentOrder(customer, order, creditCardTokenize);
		}
		if (order.getStatus().equals(Status.FINALIZED)){
			this.removeProductsInStock(order);
			order = this.orderService.createOrder(order, customer);
		}
		return order;
	}

	public Order createProductList(Order order, List<CreateItemRequest> createItemRequestList){
		if (createItemRequestList.size() > 0){
			List<Item> itemList = new ArrayList<>();
			for (CreateItemRequest createItemRequest : createItemRequestList){
				Product product = this.productService.getProductById(createItemRequest.getProductId());
				if (product.getStock() < createItemRequest.getAmount()){
					order.setStatus(Status.INSUFFICIENT_PRODUCTS);
				}
				Item item = this.createItem(createItemRequest, order);
				itemList.add(item);
			}
			order.setItems(itemList);
			order.setPrice();
		}
		return order;
	}

	public void removeProductsInStock(Order order){

		for (Item item : order.getItems()){
			Product product = item.getProduct();
			product.modifyStock(-item.getAmount());
			productService.updateProduct(product.getProductId(), product);
		}
	}

	public Item createItem(CreateItemRequest createItemRequest, Order order){

		Product product = this.productService.getProductById(createItemRequest.getProductId());
		Item item = Item.builder()
						.withItemId(UUID.randomUUID().toString())
						.withProduct(product)
						.withAmount(createItemRequest.getAmount())
						.build();
		return this.itemService.createItem(item, order);
	}
}
