package com.example.origamishop.domain.usecases.customer;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.creditcard.CreditCardService;
import com.example.origamishop.domain.usecases.order.OrderService;
import org.springframework.stereotype.Service;

@Service
public class GetCustomerUseCase implements GetCustomerInputPort {

	private CustomerService customerService;
	private OrderService orderService;
	private CreditCardService creditCardService;

	public GetCustomerUseCase(final CustomerService customerService, final OrderService orderService,
							 final CreditCardService creditCardService) {

		this.customerService = customerService;
		this.orderService = orderService;
		this.creditCardService = creditCardService;
	}

	public Customer getCustomerById(String customerId){
		Customer customer = customerService.getCustomerById(customerId);
		customer.setOrders(this.orderService.getOrdersByCustomer(customerId));
		customer.setCreditCardList(this.creditCardService.getCreditCardsByCustomer(customerId));
		return customer;
	}
}
