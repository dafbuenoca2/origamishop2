package com.example.origamishop.domain.usecases.customer;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCustomerRequest;

public interface CreateCustomerInputPort {

	Customer createCustomer(CreateCustomerRequest createCustomerRequest);
}
