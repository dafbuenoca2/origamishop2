package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.model.Product;
import com.example.origamishop.domain.usecases.category.CategoryService;
import com.example.origamishop.entrypoint.adapter.web.request.UpdateProductRequest;
import org.springframework.stereotype.Service;

@Service
public class UpdateProductUseCase implements UpdateProductInputPort{

	private  ProductService productService;
	private CategoryService categoryService;

	public UpdateProductUseCase(final ProductService productService, final CategoryService categoryService) {

		this.productService = productService;
		this.categoryService = categoryService;
	}

	@Override
	public Product updateProduct(String productId, UpdateProductRequest updateProductRequest){
		Category category = categoryService.getCategoryById(updateProductRequest.getCategory());
		Product product = Product.builder().withProductId(productId)
								 .withName(updateProductRequest.getName())
								 .withDescription(updateProductRequest.getDescription())
								 .withPrice(updateProductRequest.getPrice())
								 .withStock(updateProductRequest.getStock())
								 .withCategory(category)
								 .build();
		return productService.updateProduct(productId, product);
	}
}
