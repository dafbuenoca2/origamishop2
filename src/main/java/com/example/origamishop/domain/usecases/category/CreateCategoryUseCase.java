package com.example.origamishop.domain.usecases.category;

import java.util.UUID;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCategoryRequest;
import org.springframework.stereotype.Service;

@Service
public class CreateCategoryUseCase  implements  CreateCategoryInputPort{

	private CategoryService categoryRepository;

	public CreateCategoryUseCase(final CategoryService categoryService) {

		this.categoryRepository = categoryService;
	}

	@Override
	public Category addCategory(CreateCategoryRequest createCategoryRequest){
		Category category = Category.builder().withCategoryId(UUID.randomUUID().toString())
				.withName(createCategoryRequest.getName())
				.withDescription(createCategoryRequest.getDescription())
				.build();
		return  categoryRepository.addCategory(category);
	}
}
