package com.example.origamishop.domain.usecases.category;

import com.example.origamishop.domain.model.Category;

public interface CategoryInputPort {

	Category getCategoryById(String categoryId);
}
