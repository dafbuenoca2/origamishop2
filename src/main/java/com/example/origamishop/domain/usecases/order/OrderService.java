package com.example.origamishop.domain.usecases.order;

import java.util.List;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.model.Order;

public interface OrderService {

	Order createOrder(Order order, Customer customer);

	List<Order> getOrdersByCustomer(String customerId);

	Order getOrderById(String orderId);
}
