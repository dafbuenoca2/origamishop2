package com.example.origamishop.domain.usecases.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;

public interface GetPruductsByCategoryInputPort {

	List<Product> getProductsByCategory(String categoryId);

}
