package com.example.origamishop.domain.usecases.category;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCategoryRequest;

public interface CreateCategoryInputPort {

	Category addCategory(CreateCategoryRequest createCategoryRequest);
}
