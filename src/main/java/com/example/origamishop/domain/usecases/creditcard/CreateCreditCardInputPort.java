package com.example.origamishop.domain.usecases.creditcard;

import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCreditCardRequest;

public interface CreateCreditCardInputPort {
	CreditCardTokenize createCreditCard(String customerId, CreateCreditCardRequest createCreditCardRequest);
}
