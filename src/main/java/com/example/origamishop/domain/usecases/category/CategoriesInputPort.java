package com.example.origamishop.domain.usecases.category;

import java.util.List;

import com.example.origamishop.domain.model.Category;


public interface CategoriesInputPort {

	List<Category> getCategories();
}
