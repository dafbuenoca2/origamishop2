package com.example.origamishop.domain.usecases.customer;

import com.example.origamishop.domain.model.Customer;

public interface CustomerService {

	Customer createCustomer(Customer customer);

	Customer getCustomerById(String customer);
}
