package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Product;

public interface DeleteProductInputPort {

	Product deleteProduct(String categoryId);
}
