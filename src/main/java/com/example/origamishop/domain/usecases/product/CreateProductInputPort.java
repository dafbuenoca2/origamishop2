package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.entrypoint.adapter.web.request.CreateProductRequest;

public interface CreateProductInputPort {

	Product createProduct(CreateProductRequest createProductRequest);
}
