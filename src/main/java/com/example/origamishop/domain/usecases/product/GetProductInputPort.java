package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Product;

public interface GetProductInputPort {

	Product getProductById(String productId);
}
