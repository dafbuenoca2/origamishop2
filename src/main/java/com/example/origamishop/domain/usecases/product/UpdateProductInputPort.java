package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Product;
import com.example.origamishop.entrypoint.adapter.web.request.UpdateProductRequest;

public interface UpdateProductInputPort {

	Product updateProduct(String productId, UpdateProductRequest updateProductRequest);
}
