package com.example.origamishop.domain.usecases.payment;

import com.example.origamishop.domain.model.CreditCard;
import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;

public interface TokenizerService {

	CreditCardTokenize tokenizeCreditCard(CreditCard creditCard, Customer customer);
}
