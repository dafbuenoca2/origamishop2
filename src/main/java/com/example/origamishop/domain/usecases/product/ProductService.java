package com.example.origamishop.domain.usecases.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;

public interface ProductService {

	List<Product> getProducts();

	Product getProductById(String productId);

	Product createProduct(Product product);

	Product deleteProduct(String productId);

	Product updateProduct(String productId, Product product);

	List<Product> getProductsByCategory(String categoryId);
}
