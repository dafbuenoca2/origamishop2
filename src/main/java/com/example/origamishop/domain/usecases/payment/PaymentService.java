package com.example.origamishop.domain.usecases.payment;

import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.model.Order;

public interface PaymentService {

	Order paymentOrder(Customer customer, Order order, CreditCardTokenize cardTokenize);
}
