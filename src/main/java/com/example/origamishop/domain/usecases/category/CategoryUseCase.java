package com.example.origamishop.domain.usecases.category;

import com.example.origamishop.domain.model.Category;
import org.springframework.stereotype.Service;

@Service
public class CategoryUseCase implements CategoryInputPort{

	private CategoryService categoryRepository;

	public CategoryUseCase(final CategoryService categoryRepository) {

		this.categoryRepository = categoryRepository;
	}

	public Category getCategoryById(String categoryId){

		return categoryRepository.getCategoryById(categoryId);
	}
}
