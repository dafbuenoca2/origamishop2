package com.example.origamishop.domain.usecases.creditcard;

import java.util.List;

import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;

public interface CreditCardService {

	CreditCardTokenize createCreditCard(CreditCardTokenize creditCardTokenize, Customer customer);

	CreditCardTokenize getCreditCardId(String creditCardTokenizeId);

	List<CreditCardTokenize> getCreditCardsByCustomer(String customerId);
}
