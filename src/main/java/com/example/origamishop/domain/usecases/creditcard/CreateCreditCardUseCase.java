package com.example.origamishop.domain.usecases.creditcard;

import java.util.UUID;

import com.example.origamishop.domain.model.CreditCard;
import com.example.origamishop.domain.model.CreditCardTokenize;
import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.usecases.customer.CustomerService;
import com.example.origamishop.domain.usecases.payment.TokenizerService;
import com.example.origamishop.entrypoint.adapter.web.request.CreateCreditCardRequest;
import org.springframework.stereotype.Service;

@Service
public class CreateCreditCardUseCase implements CreateCreditCardInputPort{

	private CustomerService customerService;
	private TokenizerService tokenizerService;
	private CreditCardService creditCardService;

	public CreateCreditCardUseCase(final CustomerService customerService,
								   final TokenizerService tokenizerService,
								   final CreditCardService creditCardService) {

		this.customerService = customerService;
		this.tokenizerService = tokenizerService;
		this.creditCardService = creditCardService;
	}

	@Override
	public CreditCardTokenize createCreditCard(String customerId, CreateCreditCardRequest createCreditCardRequest){

		Customer customer =  this.customerService.getCustomerById(customerId);
		CreditCard creditCard = CreditCard.builder()
				.withCreditCardId(UUID.randomUUID().toString())
				.withAlias(createCreditCardRequest.getAlias())
				.withNumber(createCreditCardRequest.getNumberCreditCard())
				.withExpiration(createCreditCardRequest.getExpiration())
				.withPaymentMethod(createCreditCardRequest.getPaymentMethod())
				.build();
		CreditCardTokenize creditCardTokenize = this.tokenizerService.tokenizeCreditCard(creditCard, customer);
		return this.creditCardService.createCreditCard(creditCardTokenize, customer);
	}
}
