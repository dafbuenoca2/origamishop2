package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Product;
import org.springframework.stereotype.Service;

@Service
public class DeleteProductUseCase implements DeleteProductInputPort {

	private ProductService productRepository;

	public DeleteProductUseCase(final ProductService productRepository) {

		this.productRepository = productRepository;
	}

	@Override
	public Product deleteProduct(String productId) {

		return productRepository.deleteProduct(productId);
	}
}
