package com.example.origamishop.domain.usecases.category;

import java.util.List;

import com.example.origamishop.domain.model.Category;
import com.example.origamishop.domain.model.Product;

public interface CategoryService {

	List<Category> getCategories();

	Category getCategoryById(String categoryId);

	Category addCategory(Category category);

	Category removeCategory(String categoryId);

}
