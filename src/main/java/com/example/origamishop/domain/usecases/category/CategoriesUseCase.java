package com.example.origamishop.domain.usecases.category;

import java.util.List;

import com.example.origamishop.domain.model.Category;
import org.springframework.stereotype.Service;

@Service
public class CategoriesUseCase implements CategoriesInputPort {

	private CategoryService categoryRepository;

	public CategoriesUseCase(final CategoryService categoryService) {

		this.categoryRepository = categoryService;
	}

	@Override
	public List<Category> getCategories() {

		return this.categoryRepository.getCategories();
	}
}
