package com.example.origamishop.domain.usecases.order;

import com.example.origamishop.domain.model.Order;
import com.example.origamishop.entrypoint.adapter.web.request.CreateOrderRequest;

public interface CreateOrderInputPort {
	Order createOrder(String customerId, CreateOrderRequest createOrderRequest);
}
