package com.example.origamishop.domain.usecases.order;

import com.example.origamishop.domain.model.Order;

public interface RefundOrderInputPort {

	Order refundOrder(String orderId, String customerId);
}
