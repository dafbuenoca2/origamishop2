package com.example.origamishop.domain.usecases.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;

public interface GetProductsInputPort {

	List<Product> getProducts();
}
