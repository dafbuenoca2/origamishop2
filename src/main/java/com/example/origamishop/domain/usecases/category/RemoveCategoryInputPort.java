package com.example.origamishop.domain.usecases.category;

import com.example.origamishop.domain.model.Category;

public interface RemoveCategoryInputPort {

	Category removeCategory(String categoryId);
}
