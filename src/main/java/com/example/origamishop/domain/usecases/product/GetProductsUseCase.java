package com.example.origamishop.domain.usecases.product;

import java.util.List;

import com.example.origamishop.domain.model.Product;
import org.springframework.stereotype.Service;

@Service
public class GetProductsUseCase implements GetProductsInputPort {

	private ProductService productRepository;

	public GetProductsUseCase(final ProductService productService) {

		this.productRepository = productService;
	}

	@Override
	public List<Product> getProducts() {
		return productRepository.getProducts();
	}
}
