package com.example.origamishop.domain.usecases.item;

import com.example.origamishop.domain.model.Item;
import com.example.origamishop.domain.model.Order;

public interface ItemService {

	Item createItem(Item item, Order order);
}
