package com.example.origamishop.domain.usecases.product;

import com.example.origamishop.domain.model.Product;
import org.springframework.stereotype.Service;

@Service
public class GetProductUseCase implements GetProductInputPort {

	private ProductService productRepository;

	public GetProductUseCase(final ProductService productService) {

		this.productRepository = productService;
	}

	public Product getProductById(String productId){

		return productRepository.getProductById(productId);
	}
}
