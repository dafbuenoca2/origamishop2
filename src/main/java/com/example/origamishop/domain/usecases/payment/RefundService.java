package com.example.origamishop.domain.usecases.payment;

import com.example.origamishop.domain.model.Order;

public interface RefundService {

	Order refundOrder(Order order);
}
