package com.example.origamishop.domain.usecases.order;

import com.example.origamishop.domain.model.Customer;
import com.example.origamishop.domain.model.Order;
import com.example.origamishop.domain.model.Status;
import com.example.origamishop.domain.usecases.customer.CustomerService;
import com.example.origamishop.domain.usecases.payment.RefundService;
import org.springframework.stereotype.Service;

@Service
public class RefundOrderUseCase implements RefundOrderInputPort{

	OrderService orderService;
	RefundService refundService;
	CustomerService customerService;

	public RefundOrderUseCase(final OrderService orderService, final RefundService refundService, final CustomerService customerService) {

		this.orderService = orderService;
		this.refundService = refundService;
		this.customerService = customerService;
	}

	@Override
	public Order refundOrder(String orderId, String customerId){
		Order order = this.orderService.getOrderById(orderId);
		if (order.getStatus().equals(Status.FINALIZED)){
			Customer customer = this.customerService.getCustomerById(customerId);
			order = this.refundService.refundOrder(order);
			this.orderService.createOrder(order, customer	);
		}
		return order;
	}
}
